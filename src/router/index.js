import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

import myback from '@/components/myback'
// 注册
import Register from '@/components/User/Register'
// 登录
import login from '@/components/User/login'
// 分片上传
import upload_file from '@/components/Upload_file'
// 在线人数
import index from '@/components/Index'
// 汇率
import exchange_rate from '@/components/Finance/Exchange_rate'
// 支付宝支付
import payment from '@/components/Pay/payment'
import paymoney from '@/components/Pay/paymoney'

import rmborder from '@/components/Pay/rmborder'
import detail from '@/components/detail'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/myback',
      name: 'myback',
      component: myback,
      children: [
        // 注册
        {
          path: '/Register',
          name: 'Register',
          component: Register
        },
        // 登录
        {
          path: '/login',
          name: 'login',
          component: login
        },
        // 分片上传
        {
          path: '/upload_file',
          name: 'upload_file',
          component: upload_file
        },
        // 在线访问人数
        {
          path: '/index',
          name: 'index',
          component: index
        },
        // 汇率
        {
          path: '/exchange_rate',
          name: 'exchange_rate',
          component: exchange_rate
        },
        // 支付宝支付
        {
          path: '/payment',
          name: 'payment',
          component: payment
        },
        // 充值
        {
          path: '/paymoney',
          name: 'paymoney',
          component: paymoney
        },
        // 查看账单
        {
          path: '/rmborder',
          name: 'rmborder',
          component: rmborder
        },
      ]
    },


    {
      path: '/detail',
      name: 'detail',
      component: detail
    },


  ]
})

